## 1. Introduce

This is the Javascript framework for working with FIX protocol messages. Compliant with FIX 5.0 SP2.

Updated from [fixparser](https://gitlab.com/logotype/fixparser). View original README [here](docs/old-readme.md)

The Financial Information eXchange (FIX) protocol is an electronic communications protocol initiated in 1992 for international real-time exchange of information related to the securities transactions and markets.

## 2. Install package

### 2.1. Prepare Auth

#### 2.1.1. create .npmrc file

```
echo @namth0712:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
```

### 2.2. Install & Update new version

Install with alias `fixparser2`

```bash
npm i fixparser2@npm:@namth0712/fixparser2
```

## 3. Usage

```javascript
import { FIXParser } from 'fixparser';
const fixParser = new FIXParser();
console.log(
  fixParser.parse(
    '8=FIX.4.2|9=51|35=0|34=703|49=ABC|52=20100130-10:53:40.830|56=XYZ|10=249|'
  )
);
```

## 4. Features

- Parse and create FIX messages
- Connect over TCP/Websocket socket as client or server
- Fast, single-digit microsecond performance
- Modern, written in Typescript
- Validation (checksum and message length), includes FIX specification in parsed message
- Supports various separators/start of headers (e.g. 0x01, ^ and |)
- Clean and lightweight code

## 5. Message format

The general format of a FIX message is a standard header followed by the message body fields and terminated with a standard trailer.

Each message is constructed of a stream of <tag>=<value> fields with a field delimiter between fields in the stream. Tags are of data type TagNum. All tags must have a value specified. Optional fields without values should simply not be specified in the FIX message. A Reject message is the appropriate response to a tag with no value.
Except where noted, fields within a message can be defined in any sequence (Relative position of a field within a message is inconsequential.) The exceptions to this rule are:

- General message format is composed of the standard header followed by the body followed by the standard trailer.
- The first three fields in the standard header are BeginString (tag #8) followed by BodyLength (tag #9) followed by MsgType (tag #35).
- The last field in the standard trailer is the CheckSum (tag #10).
- Fields within repeating data groups must be specified in the order that the fields are specified in the message definition within the FIX specification document. The NoXXX field where XXX is the field being counted specifies the number of repeating group instances that must immediately precede the repeating group contents.
- A tag number (field) should only appear in a message once. If it appears more than once in the message it should be considered an error with the specification document. The error should be pointed out to the FIX Global Technical Committee.

In addition, certain fields of the data type MultipleCharValue can contain multiple individual values separated by a space within the "value" portion of that field followed by a single "SOH" character (e.g. "18=2 9 C<SOH>" represents 3 individual values: '2', '9', and 'C'). Fields of the data type MultipleStringValue can contain multiple values that consists of string values separated by a space within the "value" portion of that field followed by a single "SOH" character (e.g. "277=AA I AJ<SOH>" represents 3 values: 'AA', 'I', 'AJ').

It is also possible for a field to be contained in both the clear text portion and the encrypted data sections of the same message. This is normally used for validation and verification. For example, sending the SenderCompID in the encrypted data section can be used as a rudimentary validation technique. In the cases where the clear text data differs from the encrypted data, the encrypted data should be considered more reliable. (A security warning should be generated).
