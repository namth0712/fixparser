/*
 * fixparser
 * https://gitlab.com/logotype/fixparser.git
 *
 * Copyright 2019 Victor Norgren
 * Released under the MIT license
 */
import { EventEmitter } from 'events';

import { Server } from 'net';
import Field from './fields/Field';
import FIXParser from './FIXParser';
import FIXParserServerSocket from './handler/FIXParserServerSocket';
import FIXParserServerWebsocket from './handler/FIXParserServerWebsocket';
import Message from './message/Message';
import WebSocket from 'ws';

const PROTOCOL_TCP = 'tcp';
const PROTOCOL_WEBSOCKET = 'websocket';

export default class FIXServer extends EventEmitter {
  public fixParser: FIXParser = new FIXParser();
  public host = '';
  public port: number;
  public protocol = '';
  public serverHandler: FIXParserServerSocket | FIXParserServerWebsocket;
  public server: Server;
  public socket: WebSocket;
  public sender = '';
  public target = '';
  public heartBeatInterval: number;
  public heartBeatIntervalId: any;
  public fixVersion = 'FIX.5.0SP2';

  public createServer(
    host = 'localhost',
    port = 9878,
    protocol = PROTOCOL_TCP,
  ) {
    this.host = host;
    this.port = port;
    this.protocol = protocol;

    if (this.protocol === PROTOCOL_TCP) {
      this.serverHandler = new FIXParserServerSocket(
        this,
        this.fixParser,
        this.host,
        this.port,
      );
    } else if (this.protocol === PROTOCOL_WEBSOCKET) {
      this.serverHandler = new FIXParserServerWebsocket(
        this,
        this.fixParser,
        this.host,
        this.port,
      );
    } else {
      console.error(
        `[${Date.now()}] FIXServer: create server, invalid protocol`,
      );
    }
    console.log(
      `[${Date.now()}] FIXServer started at ${this.host}:${this.port}`,
    );
  }

  public getNextTargetMsgSeqNum() {
    return this.fixParser.getNextTargetMsgSeqNum();
  }

  public setNextTargetMsgSeqNum(nextMsgSeqNum: number) {
    return this.fixParser.setNextTargetMsgSeqNum(nextMsgSeqNum);
  }

  public getTimestamp(dateObject = new Date()) {
    return this.fixParser.getTimestamp(dateObject);
  }

  public createMessage(...fields: Field[]) {
    return this.fixParser.createMessage(...fields);
  }

  public parse(data: string) {
    return this.fixParser.parse(data);
  }

  public send(message: Message) {
    this.serverHandler.send(message);
  }
}
