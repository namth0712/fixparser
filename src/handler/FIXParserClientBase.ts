/*
 * fixparser
 * https://gitlab.com/logotype/fixparser.git
 *
 * Copyright 2019 Victor Norgren
 * Released under the MIT license
 */
import { EventEmitter } from 'events';

import { Socket } from 'net';
import WebSocket from 'ws';
import * as Fields from '../constants/ConstantsField';
import * as Messages from '../constants/ConstantsMessage';
import Field from '../fields/Field';
import FIXParser from '../FIXParser';
import Message from '../message/Message';

export default class FIXParserClientBase extends EventEmitter {
  public host = '';
  public port: number;
  public client: any;
  public sender = '';
  public target = '';
  public heartBeatInterval: number;
  public heartBeatIntervalId: any;
  public fixVersion = 'FIX.5.0SP2';
  protected eventEmitter: EventEmitter;
  protected fixParser: FIXParser;
  protected socketTCP: Socket;
  protected socketWS: WebSocket;

  constructor(eventEmitter: EventEmitter, parser: FIXParser) {
    super();
    this.eventEmitter = eventEmitter;
    this.fixParser = parser;
  }

  public stopHeartbeat() {
    clearInterval(this.heartBeatIntervalId);
  }

  public startHeartbeat() {
    this.stopHeartbeat();
    this.heartBeatIntervalId = setInterval(() => {
      const heartBeat = this.fixParser.createMessage(
        new Field(8, this.fixVersion),
        new Field(Fields.MsgType, 0),
        new Field(Fields.MsgSeqNum, this.fixParser.getNextTargetMsgSeqNum()),
        new Field(Fields.SenderCompID, this.sender),
        new Field(Fields.SendingTime, this.fixParser.getTimestamp()),
        new Field(Fields.TargetCompID, this.target),
      );
      this.send(heartBeat);
    }, this.heartBeatInterval);
  }

  public processMessage(message: Message) {
    if (message.messageType === Messages.SequenceReset) {
      const newSeqNo = message.getField(Fields.NewSeqNo).value;
      if (newSeqNo) {
        this.eventEmitter.emit(
          'log',
          `[${Date.now()}] FIXClient new sequence number ${newSeqNo}`,
        );
        this.fixParser.setNextTargetMsgSeqNum(newSeqNo);
      }
    } else if (message.messageType === Messages.Logon) {
      const heartBtInt = message.getField(Fields.HeartBtInt).value;
      if (heartBtInt) {
        this.eventEmitter.emit(
          'log',
          `[${Date.now()}] FIXClient new heartbeatInt ${heartBtInt}`,
        );
        this.heartBeatInterval = parseInt(heartBtInt) * 1000;
        this.startHeartbeat();
      }
    }

    this.eventEmitter.emit(
      'log',
      `[${Date.now()}] FIXClient received message ${message.description}`,
    );
  }

  public send(message: Message) {
    console.log(`[${Date.now()}] Base class ignored send()`, message.string);
  }

  public connect() {
    console.log(`[${Date.now()}] Base class ignored connect()`);
  }
}
