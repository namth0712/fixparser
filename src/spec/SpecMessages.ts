import messages from '../data/messages.json';

export interface ISpecMessages {
  ComponentID: string;
  MsgType: string;
  Name: string;
  CategoryID: string;
  SectionID: string;
  Description: string;
  AbbrName: string;
  NotReqXML: string;
  Added: string;
  AddedEP?: string;
  Updated?: string;
  UpdatedEP?: string;
}

export const MESSAGES: ISpecMessages[] = messages;
