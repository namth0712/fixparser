import fields from '../data/fields.json';

interface ISpecFields {
  Tag: string;
  Name: string;
  Type: string;
  AbbrName?: string;
  NotReqXML: string;
  Description: string;
  Added: string;
  AddedEP?: string;
  Updated?: string;
  UpdatedEP?: string;
  UnionDataType?: string;
  AssociatedDataTag?: string;
  EnumDatatype?: string;
  BaseCategory?: string;
  BaseCategoryAbbrName?: string;
  Issue?: string;
  Deprecated?: string;
}

//@ts-ignore
export const FIELDS: ISpecFields[] = fields;
