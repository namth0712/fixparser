import categories from '../data/categories.json';

export interface ISpecCategories {
  CategoryID: string;
  FIXMLFileName: string;
  NotReqXML: string;
  GenerateImplFile: string;
  ComponentType: string;
  SectionID?: string;
  Volume: string;
  IncludeFile?: string;
  Added?: string;
  AddedEP?: string;
}

export const CATEGORIES: ISpecCategories[] = categories;
