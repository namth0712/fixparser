import components from '../data/components.json';

export interface ISpecComponents {
  ComponentID: string;
  ComponentType: string;
  CategoryID: string;
  Name: string;
  AbbrName: string;
  NotReqXML: string;
  Description?: string;
  Updated?: string;
  UpdatedEP?: string;
  Added: string;
  AddedEP?: string;
}

export const COMPONENTS: ISpecComponents[] = components;
