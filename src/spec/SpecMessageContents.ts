import messageContents from '../data/messageContents.json';

export interface ISpecMessageContents {
  ComponentID: string;
  TagText: string;
  Indent: string;
  Position: string;
  Reqd: string;
  Description?: string;
  Updated?: string;
  UpdatedEP?: string;
  Added: string;
  AddedEP?: string;
  Issue?: string;
  Deprecated?: string;
}

export const MESSAGE_CONTENTS: ISpecMessageContents[] = messageContents;
