import enums from '../data/enums.json';

interface ISpecEnums {
  Tag: string;
  Value: string;
  SymbolicName: string;
  Group?: string;
  Sort: string;
  Description: string;
  Added?: string;
  AddedEP?: string;
  Updated?: string;
  UpdatedEP?: string;
  Issue?: string;
  Deprecated?: string;
  DeprecatedEP?: string;
  Elaboration?: string;
}

//@ts-ignore
export const ENUMS: ISpecEnums[] = enums;
