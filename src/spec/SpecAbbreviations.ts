import abbreviations from '../data/abbreviations.json';

interface ISpecAbbreviations {
  Term: string;
  AbbrTerm: string;
  Added: string;
  AddedEP?: string;
  Issue?: string;
}

export const ABBREVIATIONS: ISpecAbbreviations[] = abbreviations;
