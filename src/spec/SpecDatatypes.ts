import dataTypes from '../data/dataTypes.json';

export interface ISpecDatatypes {
  Name: string;
  BaseType?: string;
  Description: string;
  Example?: string;
  XML?: {
    BuiltIn: string;
    Base: string;
    MinInclusive?: string;
    Pattern?: string;
    Description?: string;
    Example?: string | string[];
  };
  Added: string;
  AddedEP?: string;
  Issue?: string;
}

export const DATATYPES: ISpecDatatypes[] = dataTypes;
