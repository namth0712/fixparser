import { ISpecEnums } from './Enums';

export class EnumType {
  public tag = '';
  public value = '';
  public symbolicName = '';
  public group = '';
  public sort = '';
  public description = '';
  public elaboration = '';
  public added = '';

  public setEnumeration(enumType: ISpecEnums) {
    this.tag = enumType.Tag;
    this.value = enumType.Value;
    this.symbolicName = enumType.SymbolicName || '';
    this.group = enumType.Group || '';
    this.sort = enumType.Sort || '';
    this.description = enumType.Description || '';
    this.elaboration = enumType.Elaboration || '';
    this.added = enumType.Added || '';
  }
}
