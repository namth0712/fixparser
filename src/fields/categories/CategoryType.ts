import { ISpecCategories } from '../../spec/SpecCategories';

export class CategoryType {
  public categoryID = '';
  public fixmlFileName = '';
  public notReqXML = '';
  public generateImplFile = '';
  public componentType = '';
  public sectionID = '';
  public volume = '';
  public includeFile = '';

  public reset() {
    this.categoryID = '';
    this.fixmlFileName = '';
    this.notReqXML = '';
    this.generateImplFile = '';
    this.componentType = '';
    this.sectionID = '';
    this.volume = '';
    this.includeFile = '';
  }

  public setCategory(category: ISpecCategories) {
    this.categoryID = category.CategoryID;
    this.fixmlFileName = category.FIXMLFileName;
    this.notReqXML = category.NotReqXML;
    this.generateImplFile = category.GenerateImplFile;
    this.componentType = category.ComponentType;
    this.sectionID = category.SectionID || '';
    this.volume = category.Volume;
    this.includeFile = category.IncludeFile || '';
  }
}
