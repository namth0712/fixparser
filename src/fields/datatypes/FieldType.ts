import { ISpecDatatypes } from '../../spec/SpecDatatypes';

export class FieldType {
  public name = '';
  public baseType = '';
  public description = '';
  public added = '';

  constructor() {
    this.reset();
  }

  public reset() {
    this.name = '';
    this.baseType = '';
    this.description = '';
    this.added = '';
  }

  public setType(type: ISpecDatatypes) {
    this.name = type.Name;
    this.baseType = type.BaseType || '';
    this.description = type.Description;
    this.added = type.Added;
  }
}
