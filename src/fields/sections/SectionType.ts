import { ISpecSections } from '../../spec/SpecSections';

export class SectionType {
  public sectionID = '';
  public name = '';
  public displayOrder = '';
  public volume = '';
  public notReqXML = '';
  public fixmlFileName = '';
  public description = '';

  public reset() {
    this.sectionID = '';
    this.name = '';
    this.displayOrder = '';
    this.volume = '';
    this.notReqXML = '';
    this.fixmlFileName = '';
    this.description = '';
  }

  public setSection(section: ISpecSections) {
    this.sectionID = section.SectionID;
    this.name = section.Name;
    this.displayOrder = section.DisplayOrder;
    this.volume = section.Volume;
    this.notReqXML = section.NotReqXML;
    this.fixmlFileName = section.FIXMLFileName;
    this.description = section.Description;
  }
}
