import { EnumType } from '../enums/EnumType';
import { CategoryType } from './categories/CategoryType';
import { FieldType } from './datatypes/FieldType';
import { SectionType } from './sections/SectionType';

export default class Field {
  public tag: number;
  public value: any;
  public name = '';
  public description = '';
  public type: FieldType = null;
  public category: CategoryType = null;
  public section: SectionType = null;
  public enumeration: EnumType = null;
  public validated = false;

  constructor(tag: number, value: any) {
    this.tag = tag >> 0;
    this.value = value;
  }

  public setTag(tag: number) {
    this.tag = tag >> 0;
  }

  public setValue(value: any) {
    this.value = value;
  }

  public setName(name: string) {
    this.name = name;
  }

  public setDescription(description: string) {
    this.description = description;
  }

  public setType(type: FieldType) {
    this.type = type;
  }

  public setCategory(category: CategoryType) {
    this.category = category;
  }

  public setSection(section: SectionType) {
    this.section = section;
  }

  public setEnumeration(enumeration: EnumType) {
    this.enumeration = enumeration;
  }

  public setValidated(isValid: boolean) {
    this.validated = isValid;
  }

  public toString(): string {
    return `${this.tag}=${this.value}`;
  }
}
