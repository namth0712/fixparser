/*
 * fixparser
 * https://gitlab.com/logotype/fixparser.git
 *
 * Copyright 2019 Victor Norgren
 * Released under the MIT license
 */
import { EventEmitter } from 'events';

import Field from './fields/Field';
import FIXParserBase from './FIXParserBase';
import FIXParserClientSocket from './handler/FIXParserClientSocket';
import FIXParserClientWebsocket from './handler/FIXParserClientWebsocket';
import Message from './message/Message';
import { timestamp } from './util/util';

type Protocol = 'tcp' | 'websocket';

export default class FIXParser extends EventEmitter {
  public fixParserBase: FIXParserBase = new FIXParserBase();
  public clientHandler: FIXParserClientSocket | FIXParserClientWebsocket;
  public host = '';
  public port: number;
  public sender = '';
  public target = '';
  public messageSequence = 1;
  public heartBeatInterval: number;
  public heartBeatIntervalId: any;
  public fixVersion = 'FIX.5.0SP2';

  public connect({
    host = 'localhost',
    port = 9878,
    protocol = 'tcp',
    sender = 'SENDER',
    target = 'TARGET',
    heartbeatIntervalMs = 30000,
    fixVersion = this.fixVersion,
  }: {
    host: string;
    port: number;
    protocol: Protocol;
    sender: string;
    target: string;
    heartbeatIntervalMs?: number;
    fixVersion: string;
  }) {
    if (protocol === 'tcp') {
      this.clientHandler = new FIXParserClientSocket(this, this);
    } else if (protocol === 'websocket') {
      this.clientHandler = new FIXParserClientWebsocket(this, this);
    }
    this.clientHandler.host = host;
    this.clientHandler.port = port;
    this.clientHandler.sender = sender;
    this.clientHandler.target = target;
    this.clientHandler.heartBeatInterval = heartbeatIntervalMs;
    this.clientHandler.fixVersion = fixVersion;
    this.clientHandler.connect();
  }

  public getNextTargetMsgSeqNum() {
    return this.messageSequence;
  }

  public setNextTargetMsgSeqNum(nextMsgSeqNum: number) {
    this.messageSequence = nextMsgSeqNum;
    return this.messageSequence;
  }

  public getTimestamp(dateObject = new Date()) {
    return timestamp(dateObject);
  }

  public createMessage(...fields: Field[]) {
    return new Message(this.fixVersion, ...fields);
  }

  public parse(data: string): Message[] {
    return this.fixParserBase.parse(data);
  }

  public send(message: Message) {
    this.clientHandler.send(message);
  }

  public close() {
    this.clientHandler.close();
  }
}

/**
 * Export global to the window object.
 */
(global as any).FIXParser = FIXParser;
